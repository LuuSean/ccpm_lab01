//1
function js_style(){
    $('#text').css({'color':'red', 'font-size':'25px'});
}
//2
$(document).ready(function(){
    $("#form_ex02").submit(function(e) {
        e.preventDefault();
        $('#result_ex02').html($('#fname').val() + " - " + $('#lname').val());
    });

    //ex10
    dynamicShowSizeWindow();
    $(window).resize(function() {
        dynamicShowSizeWindow();
    });
});
//3
function changeBG(){
    $('body').css('background-color', '#f1f1f1');
}
//4
function getAttributes(){
    $a_tag = $('#somelink');
    // alert($a_tag.attr('href'));
    $result = 'href: '+$a_tag.attr('href') + '</br>' +
        'hreflang: ' + $a_tag.attr('hreflang') + '</br>' +
        'rel: ' + $a_tag.attr('rel') + '</br>' +
        'target: ' + $a_tag.attr('target');
    $('#result_ex04').html($result);
}
//5
function addMoreRow(){
    var col1 = "new row col1";
    var col2 = "new row col2";
    var markup = "<tr><td>" + col1 + "</td><td>" + col2 + "</td></tr>";
    $("#tbl_ex05").append(markup);
}
//6
function changeContent(){
    $('#tbl_ex06 td').html($('#content').val());
}
//7
function createTable(rows, cols){
    var markup = "<tr>";
    for(i=0; i<cols; i++){
        markup += "<th> col " + i + "</th>";
    }
    markup += "</tr>";
    $("#tbl_ex07").append(markup);

    for(i=0; i<rows; i++){
        markup = "<tr>";
        for(j=0; j<cols; j++)
            markup += "<td>new row " + i + " col "+ j +"</td>";
        markup += "</tr>";
        $("#tbl_ex07").append(markup);
    }
}
//8
function deleteBottom(){
     $('#colorSelect').find('option:selected').get(0).remove();
}
//9
function showRandomImg(){
    var imgs = Array(
        "https://image.freepik.com/free-vector/abstract-blue-background_1048-1511.jpg",
        "https://png.pngtree.com/thumb_back/fh260/back_pic/00/14/65/3256657136926fa.jpg",
        "https://watermark.lovepik.com/photo/50046/8852.jpg_wh1200.jpg",
        "https://cdn1.vectorstock.com/i/1000x1000/90/65/abstract-geometric-background-with-triangle-vector-2519065.jpg"
    );
    
    var randomImg = imgs[Math.floor(Math.random()*imgs.length)];
    $('#randomImg').attr('src', randomImg);
}
//10
function dynamicShowSizeWindow(){
    var w = $(window).width();
    var h = $(window).height();
    $('h3#sizeWindows').html('Width = ' + w + " & Height = " + h);
}
//11
function fetch_api(){
    var type = "hipster-centric";
    var paras = "4";
    $.ajax({
        type: "GET",
        url: "http://www.hipsterjesus.com/api/?type="+type+"&paras="+paras,
        dataType: "json",
        success: function(data){
            var txt = data.text;
            var err = data.error;
            var msg = data.message;
            var result = "Text: "+txt + "<br>" + "Error: "+err + "<br>" + "Message: " + msg;
            $("#ajax-content").html(result);
            console.log(result);
        }
    });
}